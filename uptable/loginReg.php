<?php include('server.php');

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>UpTable</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrapValidator.css">

<style>
	body{padding-top: 3%;margin: 0;}
	.header1{background-color: #EEEEEE;padding-left: 1%;}
	.header2{padding:20px 40px 20px 40px;color:#fff;}
	.card{box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); background:#fff}
</style>

</head>
<body>

<!--Navbar menu-->
<nav class="navbar navbar-inverse navbar-fixed-top" id="my-navbar">
	<div class="container">
		<div class="navber-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a href="index.php" class="navbar-brand">UpTable</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<a href="loginReg.php" class="btn btn-info navbar-btn navbar-right" data-toggle="modal" data-target="#registerModal">Daftar</a>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="index.php">Beranda</a></li>
				<li><a href="loginReg.php" data-toggle="modal" data-target="#loginModal">Masuk</a></li>
			</ul>
		</div>		
	</div>	
</nav>
<!--End Navbar menu-->


<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-1">
            <div class="page-header">
                <h2>Masuk</h2>
            </div>
            <form id="loginForm" method="post" class="form-horizontal">
                <div style="color:red;">
                    <p><?php echo $errorMsg; ?></p>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Username</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="username" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="password" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Tipe Pengguna</label>
                    <div class="col-sm-5">
                        <div class="radio">
                            <label>
                                <input type="radio" name="usertype" value="freelancer" /> Pekerja
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="usertype" value="employer" /> Klien
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="login" class="btn btn-info btn-lg">Masuk</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6 col-md-offset-1">
            <div class="page-header">
                <h2>Daftar</h2>
            </div>

            <form id="registrationForm" method="post" class="form-horizontal">
                <div style="color:red;">
                    <p><?php echo $errorMsg2; ?></p>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Nama</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Username</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="username" value="<?php echo $username; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat Email</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" value="<?php echo $email; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="password" value="<?php echo $password; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Konfirmasi Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" name="repassword" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">No. Telepon</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="contactNo" value="<?php echo $contactNo; ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Jenis Kelamin</label>
                    <div class="col-sm-5">
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" value="Laki-Laki" /> Laki-Laki
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" value="Perempuan" /> Perempuan
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Tanggal lahir</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="birthdate" value="<?php echo $birthdate; ?>" placeholder="YYYY-MM-DD" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Alamat Lengkap</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="address" value="<?php echo $address; ?>" />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-4 control-label">Tipe Pengguna</label>
                    <div class="col-sm-5">
                        <div class="radio">
                            <label>
                                <input type="radio" name="usertype" value="freelancer" /> Pekerja
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="usertype" value="employer" /> Klien
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <!-- Do NOT use name="submit" or id="submit" for the Submit button -->
                        <button type="submit" name="register" class="btn btn-info btn-lg">Daftar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>




<script type="text/javascript" src="jquery/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="dist/js/bootstrapValidator.js"></script>


<script>
$(document).ready(function() {
    $('#registrationForm').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Nama tidak boleh kosong'
                    }
                }
            },
            username: {
                message: 'Username tidak valid',
                validators: {
                    notEmpty: {
                        message: 'Username tidak boleh kosong'
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: 'Username terdiri dari 6-30 huruf atau angka'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9]+$/,
                        message: 'Username hanya mengandung huruf dan angka'
                    },
                    different: {
                        field: 'password',
                        message: 'Username tidak boleh kosong dengan yang lainnya'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Email tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'Email tidak valid'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password tidak boleh kosong'
                    },
                    different: {
                        field: 'username',
                        message: 'Password tidak boleh sama dengan username'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Password setidaknya lebih dari 6 digit'
                    }
                }
            },
            repassword: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan konfirmasi password'
                    },
                    identical: {
                        field: 'password',
                        message: 'Password tidak boleh cocok'
                    }
                }
            },
            contactNo: {
                validators: {
                    notEmpty: {
                        message: 'No. Telepon diperlukan'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Nomor tidak valid'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'Jenis kelamin diperlukan'
                    }
                }
            },
            birthdate: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir diperlukan'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Tangga lahir tidak valid'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'Alamat dibutuhkan'
                    }
                }
            },
            usertype: {
                validators: {
                    notEmpty: {
                        message: 'Centang tipe pengguna'
                    }
                }
            }
        }
    });
    $('#loginForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                message: 'Username tidak valid',
                validators: {
                    notEmpty: {
                        message: 'Username tidak boleh kosong'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Password tidak boleh kosong'
                    }
                }
            },
            usertype: {
                validators: {
                    notEmpty: {
                        message: 'Silahkan Centang tipe pengguna'
                    }
                }
            }
        }
    });

});
</script>


</body>
</html>