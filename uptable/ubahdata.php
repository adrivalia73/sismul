<?php 
$conn = new mysqli("localhost", "root", "", "uptable");
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

 ?>
<div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h4>
          <i class="glyphicon glyphicon-edit"></i> 
          Ubah data Klien
        </h4>
      </div> <!-- /.page-header -->
      <?php
      if (isset($_GET['id'])) {
        $username   = $_GET['id'];
        $query = mysqli_query($conn, "SELECT * FROM klien WHERE username='$username'") or die('Query Error : '.mysqli_error($conn));
        while ($data  = mysqli_fetch_assoc($query)) {
        $name=$data["Name"];
    
        $address=$data["address"];
        
        $username=$data["username"];
      }
    }
      ?>
        <div class="panel panel-default">
        <div class="panel-body">
          <form class="form-horizontal" method="POST" action="proses-ubah.php">
            <div class="form-group">
              <label class="col-sm-2 control-label">NIS</label>
              <div class="col-sm-2">
                <input type="text" class="form-control" name="username" value="<?php echo $username; ?>" readonly>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Nama Klien</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="Name" autocomplete="off" value="<?php echo $name; ?>" required>
              </div>
            </div>

            <div class="form-group">
              <label class="col-sm-2 control-label">Alamat</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" name="address" autocomplete="off" value="<?php echo $address; ?>" required>
              </div>
            </div>
     
            <hr/>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-info btn-submit" name="simpan" value="Simpan">
                <a href="admin.php" class="btn btn-default btn-reset">Batal</a>
              </div>
            </div>
          </form>
        </div> <!-- /.panel-body -->
      </div> <!-- /.panel -->
    </div> <!-- /.col -->
  </div> <!-- /.row -->
